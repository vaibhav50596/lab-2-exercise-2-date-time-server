import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class DateClient {
    private Socket aSocket;
    private PrintWriter socketOut;
    private BufferedReader socketIn; //to read from socket
    private BufferedReader stdIn;

    public DateClient(String serverName, int portNumber) {
        try {
            aSocket = new Socket(serverName, portNumber);
            //keyboard input stream
            stdIn = new BufferedReader(new InputStreamReader(System.in));
            //socket input stream
            socketIn = new BufferedReader(new InputStreamReader(aSocket.getInputStream()));
            socketOut = new PrintWriter(aSocket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void communicate() {
        String line = "";
        String response = "";
        while(!line.equals("QUIT")) {
            //write to socket...
            try {
                System.out.println("Please enter a word: ");
                line = stdIn.readLine(); // read line from the user (i.e. keyboard)
                socketOut.println(line);
                response = socketIn.readLine(); //read response from socket
                System.out.println("Response is: " + response);
            } catch (IOException e) {
                e.getStackTrace();
            }
        }
        try {
            stdIn.close();
            socketIn.close();
            socketOut.close();
        } catch (IOException e) {
            e.getStackTrace();
        }
    }

    public static void main(String [] args) throws IOException {
        DateClient aClient = new DateClient("localhost", 9898);
        aClient.communicate();
    }
}
